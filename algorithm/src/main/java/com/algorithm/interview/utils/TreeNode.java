package com.algorithm.interview.utils;

public class TreeNode {
	public TreeNode left;
	public TreeNode right;
	public int data;

	public TreeNode(int data) {
		left = null;
		right = null;
		this.data = data;
	}
}