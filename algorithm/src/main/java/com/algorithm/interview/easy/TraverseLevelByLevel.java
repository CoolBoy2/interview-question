package com.algorithm.interview.easy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.algorithm.interview.utils.TreeNode;

public class TraverseLevelByLevel {
	public List<List<Integer>> binaryTreeLevelTraveral(TreeNode root){
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if(root == null) return result;
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		
		List<Integer> treeElements = new ArrayList<Integer>();
		treeElements.add(root.data);
		result.add(treeElements);
		int level = 1;
		while(!queue.isEmpty()) {
			TreeNode tree = queue.remove();
			level--;
			if(tree.left != null) queue.add(tree.left);
			if(tree.right !=null ) queue.add(tree.right);
			
			if(level == 0 && !queue.isEmpty()) {
				treeElements = new ArrayList<Integer>();
				for(TreeNode n : queue) {
					level++;
					treeElements.add(n.data);
				}
				result.add(treeElements);
			}
		}
		
		
		
		return result;
	}
}
