package com.algorithm.interview.easy;

import com.algorithm.interview.utils.TreeNode;

public class MimumDepthBT {
	public int minimum(TreeNode root) {
		if(root == null) return 0;
		return Math.min(minimum(root.left), minimum(root.right))+1;
	}
}
