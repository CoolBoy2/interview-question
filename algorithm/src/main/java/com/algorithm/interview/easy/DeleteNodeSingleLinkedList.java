package com.algorithm.interview.easy;
/*Write a function to delete a node (except the tail) in a singly linked list, given only access to that node.
Supposed the linked list is 1 -> 2 -> 3 -> 4 and you are given the third node with value 3, the linked list should become 1 -> 2 -> 4 after calling your function.
*/
public class DeleteNodeSingleLinkedList {
	public void deleteNode(Node node) {
		if(node == null) {
			return;
		}
		node.data = node.next.data;
		node.next = node.next.next;
	}

}
class Node{
	Node next;
	int data;
	Node(int data){
		this.data = data;
		next = null;
	}
}
