package com.algorithm.interview.easy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.algorithm.interview.utils.TreeNode;

public class BinaryTreeBottonUpLevelByLeve {
	public List<List<Integer>> levelByLevel(TreeNode root){
		LinkedList<List<Integer>> result = new LinkedList<>();
		result.add(null);
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		Queue<TreeNode> qq = new LinkedList<TreeNode>();
		List<Integer> l = new ArrayList<Integer>();
		if(root !=null) q.add(root);
		while(!q.isEmpty()) {
			TreeNode node = q.remove();
			l.add(node.data);
			if(q.isEmpty()) {
				result.addFirst(l);
				l.clear();
			}
			if(node.left != null) qq.add(node.left);
			if(node.right !=null) qq.add(node.right);
			
			if(q.isEmpty()) {
				q.addAll(new LinkedList<>(qq));
				qq.clear();
			}
		}
		return result;
	}
}
