package com.algorithm.interview.easy;

public class RemoveElementArray {
	public int removeElement(int[] arr, int value) {
		if(arr.length==0) {
			return 0;
		}
		int count = 0;
		for(int i=0;i<arr.length;i++) {
			if(arr[i] == value) count++;
			else arr[i-count] = arr[i];
		}
		
		return arr.length-count;
	}
}
