package com.algorithm.interview.easy;

import java.util.LinkedList;
import java.util.Queue;

import com.algorithm.interview.utils.TreeNode;

public class InvertBinaryTree {
	//using recursion
	public TreeNode invertBinaryTree(TreeNode root) {
		if (root == null)
			return null;
		swap(root);
		invertBinaryTree(root.left);
		invertBinaryTree(root.right);
		return root;
	}

	private void swap(TreeNode root) {
		TreeNode node = root.left;
		root.left = root.right;
		root.right = node;
	}
	//using iterative 
	public void usingInterative(TreeNode root) {
		if(root!=null) return;
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		while(!queue.isEmpty()) {
			TreeNode node = queue.poll();
			swap(node);
			if(node != null) queue.add(node.left);
			if(node != null) queue.add(node.right);
		}
		
	}
	
}
