package com.algorithm.interview.easy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidAnagram {
	public List<String> allWords;
	public boolean isAnagram(String str1, String str2) {
		if(str1 == null &&str2 == null) {
			return true;
		}
		if (str1.length() != str2.length())
			return false;
		char[] firstStr = str1.toCharArray();
		char[] secondStr = str2.toCharArray();
		Map<Character, Integer> map = new HashMap<>();
		for (char c : firstStr) {
			if (!map.containsKey(c)) {
				map.put(c, 1);
			} else {
				map.put(c, map.get(c) + 1);
			}
		}
		for(char c : secondStr) {
			if(map.containsKey(c)) {
				map.put(c, map.get(c)-1);
			}
			else {
				return false;
			}
		}
		
		return false;
	}
	
	public List<String> getAllAnagram(String str) {
		List<String> validAnagramForWord = new ArrayList<>();
		for(String word : allWords) {
			if(word.length() == str.length() && isAnagramUsingArray(word, str)) {
				validAnagramForWord.add(word);
			}
		}
		return validAnagramForWord;
	}
	//simple version
	public boolean isAnagramUsingArray(String str1,String str2) {
		if(str1.length() != str2.length()) {
			return false;
		}
		
		int[] allEnglishLetter = new int[26];
		for(int i=0;i<str1.length();i++) allEnglishLetter[str1.charAt(i)-'a']++;
		for(int i=0;i<str2.length();i++) {
			int index = str2.charAt(i)-'a';
			if(allEnglishLetter[index] == 0)return false;
			allEnglishLetter[index]--;
		}
		
		return true;
	}
	public static void main(String[] args) {
		System.out.println("Hello");
	}
}
