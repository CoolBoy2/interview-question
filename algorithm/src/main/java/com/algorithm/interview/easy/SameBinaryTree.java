package com.algorithm.interview.easy;

import com.algorithm.interview.utils.TreeNode;

public class SameBinaryTree {
	public boolean isSameBinaryTree(TreeNode root1, TreeNode root2) {
		if (root1 == null && root2 == null)
			return true;
		if (root1.data != root2.data) 
			return false;
		

		return isSameBinaryTree(root1.right, root2.right) && isSameBinaryTree(root1.left, root2.left);
	}
}
