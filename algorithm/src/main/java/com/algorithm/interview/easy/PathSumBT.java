package com.algorithm.interview.easy;

import com.algorithm.interview.utils.TreeNode;

public class PathSumBT {
public static void main(String[] args) {
	
}
	
	boolean result= false;
	public boolean pathSum(TreeNode root, int sum) {
		if( root == null) result = false;
		pathSum(root, sum,0);
		return result;
	}

	private void pathSum(TreeNode root, int sum, int x) {
		if(root == null) return;
		root.data+=x;
		pathSum(root.left, sum, root.data);
		pathSum(root.right, sum,root.data);
		
		if(root.right !=null && root.left !=null && root.data==sum) {
			result = true;
		}
	}

}
