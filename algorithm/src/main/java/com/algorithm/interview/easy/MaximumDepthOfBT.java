package com.algorithm.interview.easy;

import com.algorithm.interview.utils.TreeNode;

public class MaximumDepthOfBT {
	public int maximumDepth(TreeNode tree) {
		if(tree == null) return 0;
		return Math.max(maximumDepth(tree.left), maximumDepth(tree.right))+1;
	}
}
